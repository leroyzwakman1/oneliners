# Some usefull openssl commands

## Self signed certifacte for testing without a CA.
Usefull for quick testing, askes for fqdn and outputs a key file without password and crt file 
```
echo "Give a fqdn (e.g. name.domain.name):" ; read fqdn ; openssl req -x509 -newkey rsa:4096 -keyout ${fqdn}.key -out ${fqdn}.crt -days 365 -nodes -subj "/CN=${fqdn}" -addext "subjectAltName = DNS:${fqdn}"
"
```

## Fetch Cert Chain from remote
```
echo "Give a fqdn:port (e.g. name.domain.name:443):" ; read fqdn ; 
openssl s_client -showcerts -connect ${fqdn} < /dev/null | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p'
```
